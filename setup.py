# -*- coding: utf8 -*-

# ultraviolance distutils hack
import re
import distutils.versionpredicate
distutils.versionpredicate.re_validPackage = re.compile(r"^\s*([a-z_-]+)(.*)", re.IGNORECASE)

import os
from distutils.core import setup
from distutils.extension import Extension

try:
    import xpkg
except ImportError:
    pass

SRC_PATH = 'src/xsyslog'

def is_package(path):
    return (
        os.path.isdir(path) and
        os.path.isfile(os.path.join(path, '__init__.py'))
        )

def find_packages(path, base="", root_packages = None):
    """ Find all packages in path """
    packages = {}
    for item in os.listdir(path):
        dir = os.path.join(path, item)
        if is_package( dir ):
            if base:
                module_name = "%s.%s" % (base, item)
            else:
                module_name = item
            if root_packages:
                for pkg in root_packages:
                    if module_name[:len(pkg)]  == pkg:
                        break
                else:
                    # not a root package
                    continue

            packages[module_name] = dir
            packages.update(find_packages(dir, module_name))
    return packages


def src_path(*args):
    return [os.path.join(SRC_PATH,x) for x in args ]


extra = {}
try:
    from Cython.Distutils import build_ext
    extra = dict(cmdclass={'build_ext': build_ext})
    xsyslog  = Extension('xsyslog._xsyslog', src_path('_xsyslog.pyx'),
                         libraries = ['cxsyslog']
                        )
except ImportError:
    xsyslog  = Extension('xsyslog._xsyslog', src_path('_xsyslog.c'),
                         libraries = ['cxsyslog']
                        )



packages = find_packages("./src", root_packages = ['xsyslog'])

setup (
        name='xsyslog',
        version='0.1.7',
        packages=packages,
        package_dir = {'xsyslog' : SRC_PATH},
        author='Evgeny Turnaev',
        author_email='turnaev.e@gmail.com',
        description='Xsyslog',
        long_description="""Syslog improved""",

        classifiers = ['devel'],
        options={'build_pkg': {'name_prefix': True,
                               'python_min_version': 2.7,
                              }},
        ext_modules=[xsyslog],
        provides = packages.keys(),
        **extra
        )



