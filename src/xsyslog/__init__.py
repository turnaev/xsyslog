
import syslog
import imp
import sys
from ._xsyslog import xsyslog, XLog
from traceback import format_exc, print_exc

from _console import setup_logging_console, LEVELCOLOR, colorstr

xlog = None

def setup_logging(syslog_addr, logging_conf, err_stream, tag = ""):
    global xlog
    for x in ('debug', 'info', 'notice', 'warning', 'error' , 'critical', 'alert'):
        if x not in logging_conf:
            raise KeyError("Xsyslog setup error: %s key is required in setup dict." % repr(x))

    xlog = XLog(syslog_addr, err_stream.fileno())

    module = imp.new_module('logging')
    for f_name, syslog_priority in logging_conf.iteritems():
        if type(syslog_priority) == type([]):
            tmp = 0
            for x in syslog_priority:
                tmp |= getattr(syslog, x)
            syslog_priority = tmp
        f = _get_func(xlog, syslog_priority, tag, f_name)
        setattr(module, f_name, f)
    sys.modules['logging'] = module

def get_log_fds():
    global xlog
    if xlog:
        return xlog.get_fds()
    return []


def _get_func(token, syslog_priority, tag, f_name):
    color_seq = LEVELCOLOR[f_name.upper()]
    def _tmp(msg, exc_info = False, priority_facility = syslog_priority, tag = tag):
        try:
            msg = str(msg)
            if exc_info:
                msg += "\n" + format_exc()
            msg = colorstr(msg, color_seq)
            xsyslog(token, msg, priority_facility, tag)
        except:
            try:
                print_exc()
            except:
                pass
    return _tmp


__all__ = [
    'XLog',
    'xsyslog',
    'setup_logging',

    'setup_logging_console',
    'get_log_fds',
]

