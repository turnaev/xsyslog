# -*- coding: utf8 -*-

import logging
import traceback


def patch_logging(int_level, name):
    def log_helper(self, msg, *args, **kwargs):
        if self.isEnabledFor(int_level):
            self._log(int_level, msg, args, **kwargs)
    setattr(logging.Logger, name, log_helper)
    #setattr(logging.RootLogger, name, log_helper)
    #setattr(logging.root, name, log_helper)

def patch_logging_after_init():
    root = logging.getLogger()

    def notice(msg, *args, **kwargs):
        root.notice(msg, *args, **kwargs)

    def alert(msg, *args, **kwargs):
        root.alert(msg, *args, **kwargs)

    setattr(logging, 'notice', notice)
    setattr(logging, 'alert', alert)

logging.addLevelName(25, 'NOTICE')
logging.addLevelName(60, 'ALERT')
patch_logging(25, 'notice')
patch_logging(60, 'alert')


CODE= {
    # 'ENDC':0,  # RESET COLOR
    'BOLD':1,
    'UNDERLINE':4,
    'BLINK':5,
    'INVERT':7,
    'CONCEALD':8,
    'STRIKE':9,
    'GREY30':90,
    'GREY40':2,
    'GREY65':37,
    'GREY70':97,
    'GREY20_BG':40,
    'GREY33_BG':100,
    'GREY80_BG':47,
    'GREY93_BG':107,
    'DARK_RED':31,
    'RED':91,
    'RED_BG':41,
    'LIGHT_RED_BG':101,
    'DARK_YELLOW':33,
    'YELLOW':93,
    'YELLOW_BG':43,
    'LIGHT_YELLOW_BG':103,
    'DARK_BLUE':34,
    'BLUE':94,
    'BLUE_BG':44,
    'LIGHT_BLUE_BG':104,
    'DARK_MAGENTA':35,
    'PURPLE':95,
    'MAGENTA_BG':45,
    'LIGHT_PURPLE_BG':105,
    'DARK_CYAN':36,
    'AQUA':96,
    'CYAN_BG':46,
    'LIGHT_AUQA_BG':106,
    'DARK_GREEN':32,
    'GREEN':92,
    'GREEN_BG':42,
    'LIGHT_GREEN_BG':102,
    'BLACK':30,

    'WHITE' : 41 & 5,
}


def cl(name):
    return '\033[%sm' % CODE[name]

def colorstr(msg, color_seq):
    return color_seq + msg + "\033[0m"

LEVELCOLOR = {
    'DEBUG'   : cl('GREY65'),
    'INFO'    : cl('DARK_GREEN'),
    'NOTICE'  : cl('GREEN'),
    'WARNING' : cl('AQUA'),
    'ERROR'   : cl('RED'),
    'CRITICAL': cl('WHITE') + cl('RED_BG'),
    'ALERT'   : cl('WHITE') + cl('RED_BG') + cl('BLINK'),
    }


class ColoredFormatter(logging.Formatter):
    # A variant of code found at http://stackoverflow.com/questions/384076/how-can-i-make-the-python-logging-output-to-be-colored

    LEVELCOLOR = LEVELCOLOR

    def __init__(self, fmt = '%(asctime)s %(levelname)s %(process)d %(message)s'):
        logging.Formatter.__init__(self, fmt)

    def format(self, record):
        levelname = record.levelname
        if levelname in self.LEVELCOLOR:
            record.levelname = colorstr(levelname,self.LEVELCOLOR[levelname])
            record.name = colorstr(record.name,'BOLD')
            record.msg = colorstr(str(record.msg),self.LEVELCOLOR[levelname])
        ret = record
        try:
            ret = logging.Formatter.format(self, record)
        except:
            traceback.print_exc()
        return ret


LOGGING_CONFIG = {
    'version' : 1,
    'disable_existing_loggers': True,
     'formatters': {
        'colored_formatter' : {
                '()' : 'xsyslog._console.ColoredFormatter',
            'format' : '%(asctime)s %(levelname)s %(process)d %(message)s',
        },
    },
    'handlers': {
        'console':{
                'level': 'DEBUG',
                'class': 'logging.StreamHandler',
            'formatter': 'colored_formatter',
        },
    },
    'loggers': {
        '': {
            'handlers': ['console'],
            'level': 'DEBUG',
        },
    },
}

def setup_logging_console():
    import logging.config
    logging.config.dictConfig(LOGGING_CONFIG)
    patch_logging_after_init()



