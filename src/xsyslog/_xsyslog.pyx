
#from posix.unistd cimport getpid, usleep, close
#from libc.stdio cimport snprintf
#from libc.string cimport strerror
#from libc.errno cimport ENOBUFS, errno
#from cpython cimport PyErr_SetFromErrno

from cpython.string cimport PyString_Check, PyString_AS_STRING

cdef extern from "Python.h":
    ctypedef struct PyObject
    PyObject *PyExc_Exception
    PyObject *PyErr_SetFromErrno(PyObject *)

cdef extern from "cxsyslog.h":
    struct xlog_t:
        int sock_fd
        int fallback_fd

    struct log_conf_t:
        pass

    xlog_t * xopenlog(char * addr, int err_stream_fd, log_conf_t * conf)
    void xlogclose(xlog_t * log)
    int xloglog(xlog_t * log, char * msg, unsigned int facility_priority, char * tag)


cdef xlog_t * open_xlog(char * addr, int err_stream_fd) except NULL:
    cdef:
        xlog_t * xlog
    xlog = xopenlog(addr, err_stream_fd, NULL)
    if xlog == NULL:
        PyErr_SetFromErrno(PyExc_Exception)
    return xlog


cdef class XLog:
    cdef:
        xlog_t * xlog

    def __cinit__(self, char * addr, int err_stream_fd):
        self.xlog = open_xlog(addr, err_stream_fd)

    def get_fds(self):
        return [self.xlog.sock_fd, self.xlog.fallback_fd]

    def __dealloc__(self):
        xlogclose(self.xlog)

def xsyslog(XLog log, msg, unsigned int facility_priority, tag):

    if not PyString_Check(msg):
        raise TypeError("log message expected to be string got: %s instead." % type(msg))

    if not PyString_Check(tag):
        raise TypeError("log tag expected to be string got: %s instead." % type(tag))

    xloglog(log.xlog, PyString_AS_STRING(msg), facility_priority, PyString_AS_STRING(tag))


