#!/usr/bin/env python

import sys
import syslog
from xsyslog import xsyslog, open_xlog

import time

my_log = open_xlog('/dev/log', sys.stderr)

while True:
    xsyslog(my_log, ("hello " * 10) + "%s" % time.time(), syslog.LOG_DEBUG | syslog.LOG_LOCAL7)
    time.sleep(0.3)

