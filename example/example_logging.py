#!/usr/bin/env python

import sys
import syslog

logging_conf = {
    'debug'    : syslog.LOG_DEBUG | syslog.LOG_LOCAL0,
    'info'     : syslog.LOG_INFO | syslog.LOG_LOCAL0,
    'notice'   : syslog.LOG_NOTICE | syslog.LOG_LOCAL0,
    'warning'  : syslog.LOG_WARNING | syslog.LOG_LOCAL0,
    'error'    : syslog.LOG_ERR | syslog.LOG_LOCAL0,
    'critical' : syslog.LOG_CRIT | syslog.LOG_LOCAL0,
    'alert'    : syslog.LOG_ALERT | syslog.LOG_LOCAL0,
}

import xsyslog
from xsyslog import setup_logging
setup_logging('/dev/log', logging_conf, sys.stderr, tag = "xfront_cache")

from logging import debug
import time

print "logging use fds: %s" % repr(xsyslog.get_log_fds())

while True:
    debug("hello world!! %s" % time.time())
    time.sleep(0.3)

